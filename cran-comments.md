## R CMD check results

0 errors | 0 warnings | 1 note

* Minimal fixes in readme

#--------- REQUEST
* Please write references in the description of the DESCRIPTION file in the form
authors (year) <doi:...>
with no space after 'doi:' and angle brackets for auto-linking.
(If you want to add a title as well please put it in quotes: "Title") 

Response Done

#--------- REQUEST
Please ensure that your functions do not write by default or in your examples/vignettes/tests in the user's home filespace (including the package directory and getwd()). This is not allowed by CRAN policies.
Please omit any default path in writing functions. In your examples/vignettes/tests you can write to tempdir(). 
e.g.: R/coveR2.R

ESPONSE: The `R/coveR2.R` functions have an optional argument `export.image` which by default is set as FALSE. This option is needed as users can store the results of image processing to further 'external' elaboration (or keep a permanent record). In the examples the argument is not set so by default it does **not** write to home space.